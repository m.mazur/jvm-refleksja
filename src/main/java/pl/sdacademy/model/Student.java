package pl.sdacademy.model;

public class Student {
    private String imie;
    private String nazwisko;
    private int rokStudiow;
    private double srednia;
    private Promotor promotor;

    @Label("Promotor")
    public Promotor getPromotor() {
        return promotor;
    }

    @Writable("Promotor")
    public Student setPromotor(Promotor promotor) {
        this.promotor = promotor;
        return this;
    }

    @Label("Średnia ocen")
    public double getSrednia() {
        return srednia;
    }

    @Writable("Średnia ocen")
    public Student setSrednia(double srednia) {
        this.srednia = srednia;
        return this;
    }

    @Override
    public String toString() {
        return "Student{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", rokStudiow=" + rokStudiow +
                '}';
    }

    @Label("Imię")
    public String getImie() {
        return imie;
    }

    @Writable("Imię")
    public Student setImie(String imie) {
        this.imie = imie;
        return this;
    }

    @Label("Nazwisko")
    public String getNazwisko() {
        return nazwisko;
    }

    @Writable("Nazwisko")
    public Student setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
        return this;
    }

    @Label("Rok studiów")
    public int getRokStudiow() {
        return rokStudiow;
    }

    @Writable("Rok studiów")
    public Student setRokStudiow(int rokStudiow) {
        this.rokStudiow = rokStudiow;
        return this;
    }
}
