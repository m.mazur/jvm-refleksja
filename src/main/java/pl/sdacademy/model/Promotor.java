package pl.sdacademy.model;

public class Promotor {
    private String imie;
    private String nazwisko;
    private int level;

    @Label("Imie")
    public String getImie() {
        return imie;
    }

    @Writable("Imie")
    public Promotor setImie(String imie) {
        this.imie = imie;
        return this;
    }

    @Label("Nazwisko")
    public String getNazwisko() {
        return nazwisko;
    }

    @Writable("Nazwisko")
    public Promotor setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
        return this;
    }

    @Label("Level")
    public int getLevel() {
        return level;
    }

    @Writable("Level")
    public Promotor setLevel(int level) {
        this.level = level;
        return this;
    }
}
